object FileSystem {
  val getRootPath = Directory(null, null) 
}

abstract class Path(parent: Path, name: String) {
  var subLocations: List[Path] = List()
  def getParent = parent
  def getName = name
}

case class Directory(parent: Path, name: String) extends Path(parent, name) {
  def mkdir(dirName: String): Boolean = {
    val failed = subLocations.exists(_.getName == dirName)
    if (!failed) {
      subLocations = Directory(this, dirName) :: subLocations
    }
    !failed
  }
  
  def cd(dirName: String): Option[Directory] = {
    subLocations.find(_.getName == dirName) match {
      case Some(path) => path match {
        case d: Directory => Some(d)
        case _ => None 
      }
      case None => None 
    }
  }
  
  def touch(fileName: String): Boolean = {
    subLocations.find(_.getName == fileName) match {
      case Some(path) => false
      case None =>
        subLocations = File(this, fileName) :: subLocations
        true
    }
  }

  def ls(): List[(String, String)] = {
    subLocations.map { p =>
      p match {
        case d: Directory => (p.getName, "Directory")
        case f: File =>  (p.getName, "File")
      }
    }
  }

  def ls(pathName: String): List[(String, String)] = {
    subLocations.find(_.getName == pathName) match {
      case Some(path) => path match {
        case d: Directory => d.ls()
        case f: File => List((f.getName, "File"))
      }
      case None => List()
    }
  }
}

case class File(parent: Path, name: String) extends Path(parent, name) {

}

trait Command {
  def execute(): Option[Directory]
}

case class Mkdir(loc: Directory, name: String) extends Command {
  def execute() = {
    if (loc.mkdir(name)) {
      Some(loc)
    } else {
      println("mkdir fail")
      None
    }

  } 
}
case class Cd(loc: Directory, name: String) extends Command {
  def execute() = {
    val res = loc.cd(name)
    res match {
      case Some(d) =>
      case None =>
        println("cd fail")
    }
    res
  }

}
case class Touch(loc: Directory, name: String) extends Command {
  def execute() = {
    if (loc.touch(name))
      Some(loc)
    else {
      println("touch fail")
      None
    }
  }

}
case class Ls(loc: Directory, name: String) extends Command {
  def execute() = {
    if (name == null) {
      loc.ls() foreach println
      Some(loc)
    } else {
      loc.ls(name) foreach println
      Some(loc)
    }
  }

}

object FileSystemDemo extends App {
  val rootPath = FileSystem.getRootPath
  
  Mkdir(rootPath, "usr").execute()
  val usrDir = Cd(rootPath, "usr").execute().get
  assert(usrDir != null)
  Ls(rootPath, null).execute()
  Touch(usrDir, "test.txt").execute()
  Ls(usrDir, "test.txt").execute()
  Ls(rootPath, "usr").execute()
}
